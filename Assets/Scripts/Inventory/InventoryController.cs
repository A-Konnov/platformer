﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class UnityInventoryItemEvent : UnityEvent<InventoryItem> { } //типизированный эвент


public class InventoryController : MonoBehaviour
{
    [SerializeField] private InventoryAsset m_inventory;

    public UnityInventoryItemEvent OnItemAdd = new UnityInventoryItemEvent();
    public UnityInventoryItemEvent OnItemRemove = new UnityInventoryItemEvent();

    public void AddItem(InventoryItem item)
    {
        m_inventory.Add(item);
        OnItemAdd.Invoke(item);
    }

    public void RemoveItem(InventoryItem item)
    {
        m_inventory.Remove(item);
        OnItemRemove.Invoke(item);
    }

    public bool HasItems(params InventoryItem[] items)
    {
        return m_inventory.HasItems(items);
    }

}
