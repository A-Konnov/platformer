﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Inventory/Inventory")]
public class InventoryAsset : ScriptableObject, ISerializationCallbackReceiver, IEnumerable
{
    [SerializeField] private InventoryItem[] m_defaultItems;

    [NonSerialized]
    private List<InventoryItem> m_runtimeItems = new List<InventoryItem>();

    public bool HasItems(params InventoryItem[] items)
    {
        foreach (var item in items)
        {
            if (!m_runtimeItems.Contains(item)) //проверяем наличие
            {
                return false;
            }
        }
        return true;
    }

    public void Add (InventoryItem item)
    {
        if (!m_runtimeItems.Contains(item)) //если предмета в инвентаре нет, то мы его добовляем
        {
            m_runtimeItems.Add(item);
        }
    }

    public void Remove(InventoryItem item)
    {
        if (m_runtimeItems.Contains(item)) //если предмета в инвентаре есть, то его удалить
        {
            m_runtimeItems.Remove(item);
        }
    }

    public void OnAfterDeserialize() //От ISerializationCallbackReceiver
    {
        m_runtimeItems.Clear();
        m_runtimeItems.AddRange(m_defaultItems); //перекидывает в инвентрать дефолтный инвентарь        
    }

    public void OnBeforeSerialize()
    {
        
    }

    public IEnumerator GetEnumerator() //От IEnumerable получение items
    {
        foreach (var item in m_runtimeItems)
        {
            yield return item;
        }
    }
}
