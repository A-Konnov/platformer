﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage { get; set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var hp = collision.GetComponent<Health>();
        if (hp != null)
        {
            hp.ApplyDamage(Damage);
        }

        Destroy(gameObject);
    }
}
