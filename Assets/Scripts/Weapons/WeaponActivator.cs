﻿using UnityEngine;

[RequireComponent(typeof(InventoryController))]
[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(RangeWeapon))]
public class WeaponActivator : MonoBehaviour
{
    [SerializeField] private InventoryItem m_requiredMeleeItem;
    [SerializeField] private InventoryItem m_requiredRangeItem;

    private InventoryController m_inventory;
    private MeleeWeapon m_melee;
    private RangeWeapon m_range;

    private void Start()
    {
        m_inventory = GetComponent<InventoryController>();
        m_melee = GetComponent<MeleeWeapon>();
        m_range = GetComponent<RangeWeapon>();

        m_melee.enabled = m_inventory.HasItems(m_requiredMeleeItem);
        m_range.enabled = m_inventory.HasItems(m_requiredRangeItem);

        m_inventory.OnItemAdd.AddListener(OnItemAdded);
        m_inventory.OnItemRemove.AddListener(OnItemRemoved);
    }

    private void OnDestroy()
    {
        m_inventory.OnItemAdd.RemoveListener(OnItemAdded);
        m_inventory.OnItemRemove.RemoveListener(OnItemRemoved);
    }

    private void OnItemAdded(InventoryItem item)
    {
        if(m_requiredMeleeItem == item)
        {
            m_melee.enabled = true;
        }

        if (m_requiredRangeItem == item)
        {
            m_range.enabled = true;
        }
    }

    private void OnItemRemoved(InventoryItem item)
    {
        if (m_requiredMeleeItem == item)
        {
            m_melee.enabled = false;
        }

        if (m_requiredRangeItem == item)
        {
            m_range.enabled = false;
        }
    }
}
