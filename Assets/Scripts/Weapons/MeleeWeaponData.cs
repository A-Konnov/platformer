﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MeleeWeaponData : AWeaponData
{
    public float Range = 2f; //раждиус атаки
    public float HitDelay = 0.1f; //время атаки в анимации

    public override void Attack(Transform firePoint)
    {
        var origin = firePoint.position;
        var direction = firePoint.right; //в 2D right это форвард

        var hit = Physics2D.Raycast(origin, direction, Range, Mask);
        if (hit.collider != null)
        {
            var hp = hit.collider.GetComponent<Health>();
            if (hp != null)
            {
                hp.ApplyDamage(Damage);
            }
        }

    }
}
