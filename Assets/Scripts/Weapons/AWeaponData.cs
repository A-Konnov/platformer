﻿using UnityEngine;

public abstract class AWeaponData : ScriptableObject
{
    public LayerMask Mask;
    public float Damage = 1f;
    public float FireRate = 0.5f;

    public abstract void Attack(Transform firepPoint);
}
