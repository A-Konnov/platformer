﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RangeWeaponData : AWeaponData
{
    [SerializeField] private Bullet m_ammo;
    [SerializeField] private float m_bulletSpeed;
    [SerializeField] private float m_destroyBulletTime;

    public override void Attack(Transform firepPoint)
    {
        var instance = Instantiate(m_ammo, firepPoint.position, firepPoint.rotation); //создаем пулю
        instance.Damage = Damage; //указываем пуле дамаг


        var rb2D = instance.GetComponent<Rigidbody2D>();
        rb2D.gravityScale = 0; //убиваем гравитацию
        rb2D.velocity = firepPoint.right * m_bulletSpeed; //даем пуле скорость

        Destroy(instance.gameObject, m_destroyBulletTime); //удаляем пулю
    }
}
