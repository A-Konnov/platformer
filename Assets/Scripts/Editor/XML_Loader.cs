﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Reflection;
using System;
using Object = UnityEngine.Object;

public class XML_Loader : EditorWindow
{
    public static Object Sprite;
    public static Object XML;

    [MenuItem("Tools/Create Sprite From Selection")]
    public static void LoadXML()
    {
        var selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets); //доступ к выбранному

        foreach (var item in selection)
        {
            var imagePath = AssetDatabase.GetAssetPath(item); //находим местоположение ассета в unity
            var xmlPath = imagePath.Replace("Assets/", "").Replace("png", "xml"); //переделываем путь к файлу
            xmlPath = Path.Combine(Application.dataPath, xmlPath); //делаем новый путь к файлу

            SetSprites(imagePath, xmlPath);
        }
    }

    [MenuItem("Window/Slice Sprite On XML FIle")]
    public static void ShowWindow()
    {
        GetWindow<XML_Loader>("Slice XML");
    }

    private void OnGUI()
    {
        GUILayout.Label("Slice Sprite On XML FIle", EditorStyles.boldLabel);
        Sprite = EditorGUILayout.ObjectField("Sprite to be cut", Sprite, typeof(Object), true);
        XML = EditorGUILayout.ObjectField("XML file", XML, typeof(Object), true);

        if (GUILayout.Button("Slice"))
        {
            if (Sprite != null)
            {
                var imagePath = AssetDatabase.GetAssetPath(Sprite);
                var xmlPath = AssetDatabase.GetAssetPath(XML);
                xmlPath = Path.Combine(Application.dataPath, xmlPath.Replace("Assets/", ""));
                SetSprites(imagePath, xmlPath);
            }
        }

        if (GUILayout.Button("Clear Values"))
        {
            Sprite = null;
            XML = null;
        }
    }


    private static void SetSprites(string imagePath, string xmlPath)
    {
        if (!File.Exists(imagePath)) //проверяем, есть ли файл
        {
            Debug.LogError("Image not found");
            return;
        }

        if (!File.Exists(xmlPath)) //проверяем, есть ли файл
        {
            Debug.LogError("XML File not found");
            return;
        }

        var texture2D = LoadTexture(imagePath);
        if (texture2D == null)
        {
            Debug.LogError("File is not an Image");
            return;
        }

        var xmlDoc = new XmlDocument(); //создаем xml документ
        xmlDoc.Load(xmlPath); //загружаем

        var container = new List<SubTexture>();
        foreach (XmlNode node in xmlDoc.GetElementsByTagName("SubTexture"))
        {
            var subTexture = new SubTexture();
            subTexture.AddAtributes(node.Attributes); //загружаем атрибуты
            container.Add(subTexture);
        }

        var ti = AssetImporter.GetAtPath(imagePath) as TextureImporter; //получаем TextureImporter
        if (ti == null)
        {
            return;
        }

        ti.isReadable = true;
        ti.spriteImportMode = SpriteImportMode.Single;
        ti.spriteImportMode = SpriteImportMode.Multiple;
        var smdData = new List<SpriteMetaData>(); //массив метадат спрайтов

        foreach (var subTex in container) //заполняем метаданными
        {
            var smd = new SpriteMetaData();
            smd.name = subTex.Name;
            smd.alignment = (int)SpriteAlignment.Custom;

            var pivotX = (subTex.Width * 0.5f + subTex.FrameX) / subTex.Width; //нормализованное значение центра спрайта
            var pivotY = (subTex.Height * 0.5f - subTex.FrameY) / subTex.Height;
            smd.pivot = new Vector2(pivotX, pivotY);
            smd.rect = new Rect(subTex.X, texture2D.height - subTex.Y - subTex.Height, subTex.Width, subTex.Height);
            smdData.Add(smd);
        }

        ti.spritesheet = smdData.ToArray(); //добавили сетку
        AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate); //обновляем всё по пути

    }

    private static Texture2D LoadTexture(string path)
    {
        if (File.Exists(path))
        {
            var fileData = File.ReadAllBytes(path); //прочитали данные в виде массива
            var text2D = new Texture2D(1, 1);// создаем новую текстуру
            if (text2D.LoadImage(fileData)) //загружаем данные в текстуру
            {
                return text2D;
            }
        }

        return null;
    }

    private class SubTexture
    {
        public string Name;
        public int FrameHeight;
        public int FrameWidth;
        public int FrameX;
        public int FrameY;
        public int Height;
        public int Width;
        public int X;
        public int Y;

        public void AddAtributes(XmlAttributeCollection attributes)
        {
            foreach (XmlAttribute attribute in attributes) //проходимся по всем атрибутам
            {
                AddAttribute(attribute);
            }
        }

        //Reflection
        private void AddAttribute(XmlAttribute attribute)
        {
            var fields = GetType().GetFields(); //получили массив филдов (переменных)

            foreach (var field in fields)
            {
                if (string.Equals(attribute.Name, "Name", System.StringComparison.InvariantCultureIgnoreCase)) //сравниваем атрибут в xml с филд игнорируя caps
                {
                    Name = attribute.Value;
                    continue;
                }

                if (!string.Equals(attribute.Name, field.Name, System.StringComparison.InvariantCultureIgnoreCase)) //сравниваем атрибут в xml с любым филдом игнорируя caps
                {
                    continue;
                }

                var val = 0;
                if (int.TryParse(attribute.Value, out val)) //парсим string в int
                {
                    field.SetValue(this, val); //записываем 
                }
            }
        }
    }
}
