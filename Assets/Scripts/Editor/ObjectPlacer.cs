﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class ObjectPlacer //ставит объект на поверхность
{
    [MenuItem("Tools/Place to ground %#F6")]
    private static void PlaceToGround()
    {
        var objects = Selection.gameObjects;
        foreach (var obj in objects)
        {
            PlaceObject(obj);
        }
    }

    private static void PlaceObject(GameObject obj)
    {
        var colliders = obj.GetComponents<Collider2D>();
        if (colliders.Length == 0)
        {
            return;
        }

        var minY = float.MaxValue;
        foreach (var collider in colliders)
        {
            if (collider.bounds.min.y < minY)
            {
                minY = collider.bounds.min.y;
            }
        }

        var origin = obj.transform.position;
        origin.y = minY - 0.01f; //чтоб рэйкас не попадал сам в себя

        var hit = Physics2D.Raycast(origin, Vector2.down); //рэйкас вниз
        if (hit.collider == null)
        {
            return;
        }

        var pos = hit.point; //точка врезания луча рэйкаста

        pos.y += obj.transform.position.y - minY; //на сколько нужно опустить
        obj.transform.position = pos;
    }
}
