﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Reflection;
using System;
using Object = UnityEngine.Object;

public class Window_SliceSpriteOnTheGrid : EditorWindow
{
    public static Object Sprite;
    public static int Columns;
    public static int Rows;
    public static float PivotX;
    public static float PivotY;


    [MenuItem("Window/Slice Sprite On The Grid")]
    public static void ShowWindow()
    {
        GetWindow<Window_SliceSpriteOnTheGrid>("Slice Sprite Grid");
    }

    private void OnGUI()
    {
        GUILayout.Label("Slice Sprite On The Grid", EditorStyles.boldLabel);
        Sprite = EditorGUILayout.ObjectField("Sprite to be cut", Sprite, typeof(Object), true);

        Columns = EditorGUILayout.IntField("Columns", Mathf.Clamp(Columns, 1, 100));
        Rows = EditorGUILayout.IntField("Rows", Mathf.Clamp(Rows, 1, 100));

        GUILayout.Label("Pivots", EditorStyles.boldLabel);
        PivotX = EditorGUILayout.Slider("Pivot X", PivotX, 0, 1);
        PivotY = EditorGUILayout.Slider("Pivot Y", PivotY, 0, 1);

        if (GUILayout.Button("Slice"))
        {
            if (Sprite != null)
            {
                SetSprites();
            }
        }

        if (GUILayout.Button("Clear Values"))
        {

            Sprite = null;
            Columns = 1;
            Rows = 1;
            PivotX = 0;
            PivotY = 0;
        }
    }

    private static void SetSprites()
    {
        var imagePath = AssetDatabase.GetAssetPath(Sprite);
        var texture2D = LoadTexture(imagePath);

        var ti = AssetImporter.GetAtPath(imagePath) as TextureImporter; //получаем TextureImporter
        if (ti == null)
        {
            return;
        }

        ti.isReadable = true;
        ti.spriteImportMode = SpriteImportMode.Single;
        ti.spriteImportMode = SpriteImportMode.Multiple;
        var smdData = new List<SpriteMetaData>(); //массив метадат спрайтов
        int serialNumber = 0;

        for (int curRow = 0; curRow < Rows; curRow++)
        {
            for (int curColumn = 0; curColumn < Columns; curColumn++)
            {
                var smd = new SpriteMetaData();
                smd.name = Sprite.name + "_" + serialNumber;
                serialNumber++;
                smd.alignment = (int)SpriteAlignment.Custom;
                smd.pivot = new Vector2(PivotX, PivotY);

                var width = texture2D.width / Columns;
                var height = texture2D.height / Rows;
                var X = width * curColumn;
                var Y = height * (Rows - curRow - 1);

                smd.rect = new Rect(X, Y, width, height);
                smdData.Add(smd);
            }
        }

        ti.spritesheet = smdData.ToArray(); //добавили сетку
        AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate); //обновляем всё по пути
    }

    private static Texture2D LoadTexture(string path)
    {
        if (File.Exists(path))
        {
            var fileData = File.ReadAllBytes(path);
            var text2D = new Texture2D(1, 1);
            if (text2D.LoadImage(fileData))
            {
                return text2D;
            }
        }
        return null;
    }
}
