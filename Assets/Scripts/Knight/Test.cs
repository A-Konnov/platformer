﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Test : MonoBehaviour
{
    //проверка на добавление
    //public RuntimeSet m_enemies;

    //private void Start()
    //{
    //    foreach (var enemy in m_enemies)
    //    {
    //        Debug.Log(enemy.name);
    //    }
    //}

    public Vector3 point1;
    public Vector3 point2;
    public float duration = 1f;
    public Ease Ease;
    public AnimationCurve curve;

    private void Start()
    {
        var anim1 = transform.DOMove(point1, duration).SetEase(Ease);
        var anim2 = transform.DOMove(point2, duration).SetEase(curve);
        var seq = DOTween.Sequence(); //порядок выполнение
        seq.Append(anim1);
        seq.AppendCallback(Print); //эвент
        seq.AppendInterval(1f); //задержка
        seq.Append(anim2);
        seq.Play().SetLoops(-1, LoopType.Yoyo);

        //var anim = transform.DOMove(point1, duration).SetEase(Ease).SetLoops(-1, LoopType.Yoyo);
        // anim.OnStepComplete(Print);
    }

    private void Print()
    {
        Debug.Log("HI!");
    }

}
