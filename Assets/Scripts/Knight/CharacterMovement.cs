﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float m_speed = 5;
    private Rigidbody2D m_rb2d;
    private Animator m_animator;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        m_rb2d.velocity = Vector2.right * Input.GetAxis("Horizontal") * m_speed;
        if (m_rb2d.velocity.x != 0)
        {
            m_animator.SetBool("Walk", true);
        }
        else
        {
            m_animator.SetBool("Walk", false);
        }
    }
}
