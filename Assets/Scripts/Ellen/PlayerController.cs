﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HorizontalMovement))]
[RequireComponent(typeof(Jumping))]
[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(RangeWeapon))]
public class PlayerController : MonoBehaviour
{
    private HorizontalMovement m_movement;
    private Jumping m_jumping;
    private MeleeWeapon m_meleeWeapon;
    private RangeWeapon m_rangeWeapon;

    private void Start()
    {
        m_movement = GetComponent<HorizontalMovement>();
        m_jumping = GetComponent<Jumping>();
        m_meleeWeapon = GetComponent<MeleeWeapon>();
        m_rangeWeapon = GetComponent<RangeWeapon>();
    }

    private void FixedUpdate()
    {
        m_movement.Move(Input.GetAxis("Horizontal"));
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            m_jumping.Jump();
        }

        if (Input.GetButtonDown("Fire2")) //правая кнопка мыши
        {
            m_meleeWeapon.Attack();
        }

        if (Input.GetButtonDown("Fire1")) //левая кнопка мыши
        {
            m_rangeWeapon.Attack();
        }
    }

    private void OnDisable()
    {
        m_movement.Move(0);
    }
}