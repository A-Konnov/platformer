﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMB_Movement : StateMachineBehaviour
{
    private HorizontalMovement m_movement;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_movement == null)
        {
            m_movement = animator.GetComponentInParent<HorizontalMovement>();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_movement != null)
        {
            var speed = Mathf.Abs(m_movement.Velocity.x); //скорость движения родителя
            animator.SetFloat("Speed", speed);
        }
    }
}