﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMB_Hit : StateMachineBehaviour
{
    private Health m_hp;
    private Animator m_animator;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_hp == null)
        {
            m_hp = animator.GetComponentInParent<Health>();
        }

        if (m_hp != null)
        {
            m_animator = animator;
            m_hp.OnHpChange.AddListener(OnHpChanged);
        }       
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_hp != null)
        {
            m_hp.OnHpChange.RemoveListener(OnHpChanged);
        }
    }

    private void OnHpChanged()
    {
        if (m_hp.HP > 0)
        {
            m_animator.SetTrigger("Hit");
        }
    }
}
