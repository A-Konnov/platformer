﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMB_Attack : StateMachineBehaviour
{
    private MeleeWeapon m_weapon;
    private Animator m_animator;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_weapon == null)
        {
            m_weapon = animator.GetComponentInParent<MeleeWeapon>();
        }

        if (m_weapon != null)
        {
            m_animator = animator;
            m_weapon.AttackStarted.AddListener(OnAttackStarted);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_weapon != null)
        {
            m_weapon.AttackStarted.RemoveListener(OnAttackStarted);
        }
    }

    private void OnAttackStarted()
    {
        m_animator.SetTrigger("Attack");
    }
}
