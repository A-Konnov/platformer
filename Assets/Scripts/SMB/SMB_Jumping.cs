﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMB_Jumping : StateMachineBehaviour
{
    private Jumping m_jumping;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_jumping == null)
        {
            m_jumping = animator.GetComponentInParent<Jumping>();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_jumping == null)
        {
            return;
        }

        animator.SetBool("IsGrounded", m_jumping.IsGrounded);
    }
}