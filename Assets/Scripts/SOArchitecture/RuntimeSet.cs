﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "SOArchitecture/RuntimeSet", fileName = "NewRuntimeSet")]
public class RuntimeSet : ScriptableObject, IEnumerable<GameObject> //типизируем
{
    private List<GameObject> m_set = new List<GameObject>();

    public List<GameObject> Set { get { return m_set; } }

    private void OnEnable()
    {
        m_set.Clear(); //очищаем при старте
    }

    public void Register(GameObject item)
    {
        if (!m_set.Contains(item)) //добавляем, если не содержит
        {
            m_set.Add(item);
        }
    }

    public void Unregister(GameObject item)
    {
        if (m_set.Contains(item)) //удаляем, если содержит
        {
            m_set.Remove(item);
        }
    }

    //итерируем, чтобы не предоставлять доступ напрямую к List, доступ только на чтение
    IEnumerator IEnumerable.GetEnumerator()
    {
        yield return GetEnumerator();
    }

    public IEnumerator<GameObject> GetEnumerator()
    {
        foreach (var item in m_set)
        {
            yield return item;
        }
    }
}
