﻿using UnityEngine;

public abstract class FSMAction : ScriptableObject
{
    public abstract void StartAction(FSMController fsm, float dt);
    public abstract void UpdateAction(FSMController fsm, float dt);
    public abstract void CompleteAction(FSMController fsm, float dt);

    public virtual void DrawGizmos(FSMController fsm)
    {

    }
}
