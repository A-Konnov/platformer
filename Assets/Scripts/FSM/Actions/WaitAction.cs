﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Action/Wait", fileName = "NewWaitAction")]
public class WaitAction : FSMAction
{
    public override void CompleteAction(FSMController fsm, float dt)
    {

    }

    public override void StartAction(FSMController fsm, float dt)
    {

    }

    public override void UpdateAction(FSMController fsm, float dt)
    {

    }
}
