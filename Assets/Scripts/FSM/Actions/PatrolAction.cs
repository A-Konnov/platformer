﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "AI/Action/Patrol", fileName = "NewPatrolAction")]
public class PatrolAction : FSMAction
{
    public override void CompleteAction(FSMController fsm, float dt)
    {
        
    }

    public override void StartAction(FSMController fsm, float dt)
    {
        SetNextWaypoint(fsm);
    }

    public override void UpdateAction(FSMController fsm, float dt)
    {
        MoveThrowWaypoints(fsm, dt);
    }

    private void MoveThrowWaypoints(FSMController fsm, float dt)
    {
        var wayPoint = fsm.WayPoints[fsm.CurrWayPoint];
        var moveDir = wayPoint.x > fsm.transform.position.x ? 1 : -1;

        fsm.Movement.Move(moveDir);

        var frameSpeed = fsm.Movement.Speed * dt;
        if (Mathf.Abs(wayPoint.x - fsm.transform.position.x) < frameSpeed)
        {
            SetNextWaypoint(fsm);
        }
    }

    private static void SetNextWaypoint(FSMController fsm)
    {
        fsm.CurrWayPoint++;
        fsm.CurrWayPoint = (int)Mathf.PingPong(fsm.CurrWayPoint, fsm.WayPoints.Length - 1);
    }

    public override void DrawGizmos(FSMController fsm)
    {
        var points = new List<Vector2>(fsm.WayPoints);

        if(!Application.isPlaying)
        {
            for (int i=0; i< points.Count; i++)
            {
                points[i] += (Vector2)fsm.transform.position;
            }
        }

        for (int i=0; i<points.Count - 1; i++)
        {
            var point1 = points[i];
            var point2 = points[i + 1];
            Gizmos.DrawSphere(point1, 0.2f);
            Gizmos.DrawSphere(point2, 0.2f);
            Gizmos.DrawLine(point1, point2);
        }
    }
}
