﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMController : MonoBehaviour
{
    [SerializeField] private FSMState m_initialState;

    public HorizontalMovement Movement;
    public int CurrWayPoint;
    public Vector2[] WayPoints;
    public float WaitingTime = 1.5f;

    private FSMState m_currState;

    public float StateTimer;

    private void Start()
    {
        for (int i = 0; i < WayPoints.Length; i++)
        {
            WayPoints[i] += (Vector2)transform.position;
        }

        m_currState = m_initialState;
        if (m_currState != null)
        {
            m_currState.EnterState(this, Time.deltaTime);
        }
    }

    private void Update()
    {
        if (m_currState != null)
        {
            m_currState.UpdateState(this, Time.deltaTime);
        }
    }

    private void OnDestroy()
    {
        if (m_currState != null)
        {
            m_currState.ExitState(this, Time.deltaTime);
        }
    }

    public void TransitionToState(FSMState newState)
    {
        if (newState == null || newState == m_currState)
        {
            return;
        }

        if (m_currState != null)
        {
            m_currState.ExitState(this, Time.deltaTime);
        }

        m_currState = newState;
        m_currState.EnterState(this, Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        if (m_currState != null)
        {
            m_currState.DrawGizmos(this);
        }
        else if (m_initialState != null)
        {
            m_initialState.DrawGizmos(this);
        }
    }
}
