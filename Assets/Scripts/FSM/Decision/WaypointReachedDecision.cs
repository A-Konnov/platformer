﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/WaypointReached", fileName = "WaypointReached")]
public class WaypointReachedDecision : FSMDecision
{
    public float Epsilon = 0.1f;

    public override bool Decide(FSMController fsm)
    {
        var point = fsm.WayPoints[fsm.CurrWayPoint];
        var xDistance = Mathf.Abs(point.x - fsm.transform.position.x);
        return xDistance <= Epsilon;
    }
}
