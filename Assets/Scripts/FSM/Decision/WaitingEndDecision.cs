﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/WaitingEnd", fileName = "WaitingEnd")]
public class WaitingEndDecision : FSMDecision
{
    public override bool Decide(FSMController fsm)
    {
        return fsm.StateTimer >= fsm.WaitingTime;
    }
}
