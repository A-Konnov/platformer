﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "AI/State", fileName = "NewState")]
public class FSMState : ScriptableObject
{
    [Serializable]
    public struct FSMTransition
    {
        public FSMDecision[] Decisions;
        public FSMState TrueState;
        public FSMState FalseState;

        public bool Evaluate(FSMController fsm)
        {
            foreach (var decision in Decisions)
            {
                if(!decision.Decide(fsm))
                {
                    return false;
                }
            }

            return true;
        }
    }

    public FSMAction[] Actions;
    public FSMTransition[] Transitions;

    # region State Loop
    public void EnterState(FSMController fsm, float dt)
    {
        fsm.StateTimer = 0;
        StartActions(fsm, dt);
    }

    public void UpdateState(FSMController fsm, float dt)
    {
        fsm.StateTimer += dt;
        UpdateActions(fsm, dt);
        UpdateTransition(fsm);
    }

    public void ExitState(FSMController fsm, float dt)
    {
        fsm.StateTimer = 0;
        CompleteActions(fsm, dt);
    }
    #endregion

    #region Action
    private void StartActions(FSMController fsm, float dt)
    {
        foreach(var action in Actions)
        {
            action.StartAction(fsm, dt);
        }
    }

    private void UpdateActions(FSMController fsm, float dt)
    {
        foreach (var action in Actions)
        {
            action.UpdateAction(fsm, dt);
        }
    }

    private void CompleteActions(FSMController fsm, float dt)
    {
        foreach (var action in Actions)
        {
            action.CompleteAction(fsm, dt);
        }
    }
    #endregion

    #region Transition
    private void UpdateTransition(FSMController fsm)
    {
        foreach (var transition in Transitions)
        {
            var result = transition.Evaluate(fsm);
            if (result)
            {
                fsm.TransitionToState(transition.TrueState);
                return;
            }
            else if (transition.FalseState != null)
            {
                fsm.TransitionToState(transition.FalseState);
                return;
            }
        }
    }
    #endregion

    #region Debug
    public void DrawGizmos(FSMController fsm)
    {
        foreach (var action in Actions)
        {
            action.DrawGizmos(fsm);
        }
    }
    #endregion
}
