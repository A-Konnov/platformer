﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelGenerator))]
public class SpawnEnemies : MonoBehaviour
{
    [SerializeField] private RuntimeSet m_tag;
    private List<GameObject> m_spawns;
    private LevelGenerator m_levelGenerator;
    private int m_enemiesAmount;

    private void Start()
    {
        m_levelGenerator = GetComponent<LevelGenerator>();
        m_spawns = m_tag.Set;

        switch (m_levelGenerator.ChunkSet.Difficult)
        {
            case ChunkSet.DifficultLevel.Easy:
                m_enemiesAmount = (int)Mathf.Floor(m_spawns.Count * m_levelGenerator.ChunkSet.DifficulEasy);
                break;
            case ChunkSet.DifficultLevel.Normal:
                m_enemiesAmount = (int)Mathf.Floor(m_spawns.Count * m_levelGenerator.ChunkSet.DifficulNormal);
                break;
            case ChunkSet.DifficultLevel.Hard:
                m_enemiesAmount = (int)Mathf.Floor(m_spawns.Count * m_levelGenerator.ChunkSet.DifficulHard);
                break;
            default:
                Debug.LogError("Not set Difficult Level");
                return;
        }
        
        for (int i = 0; i < m_enemiesAmount; i++)
        {
            var indexSpawn = Random.Range(0, m_spawns.Count);
            var indexEnemie = Random.Range(0, m_levelGenerator.ChunkSet.Enemies.Length);
            var enemie = m_levelGenerator.ChunkSet.Enemies[indexEnemie];
            var spawn = m_spawns[indexSpawn];
            var root = spawn.transform.parent;

            Instantiate(enemie, spawn.transform.position, spawn.transform.rotation, root.transform);
            Destroy(spawn);
        }
    }

}
