﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class ChunkSet : ScriptableObject
{
    public enum RepeatType
    {
        Repeat,
        Random
    }

    public enum DifficultLevel
    {
        Easy,
        Normal,
        Hard
    }

    [Header("Property Chunk")]
    [SerializeField] [Range(2, 100)] private int m_size = 2; //размер уровня, Range - ограничивает ввод
    [SerializeField] private RepeatType m_repeat = RepeatType.Repeat;
    [SerializeField] private LevelChunk m_firstChunk;
    [SerializeField] private LevelChunk m_lastChunk;
    [SerializeField] private LevelChunk[] m_chunks;

    [Header("Property Enemies in Chunk")]
    [SerializeField] private GameObject[] m_enemies;
    [SerializeField] private DifficultLevel m_difficultLevel = DifficultLevel.Normal;

    [Header("Property Difficul in Chunk")]
    [SerializeField] [Range(0, 1)] private float m_difficultEasy = 0.3f;
    [SerializeField] [Range(0, 1)] private float m_difficultNormal = 0.5f;
    [SerializeField] [Range(0, 1)] private float m_difficultHard = 1;

    public GameObject[] Enemies { get { return m_enemies; } }
    public DifficultLevel Difficult { get { return m_difficultLevel; } }
    public int Size { get { return m_size; } }
    public float DifficulEasy { get { return m_difficultEasy; } }
    public float DifficulNormal { get { return m_difficultNormal; } }
    public float DifficulHard { get { return m_difficultHard; } }

    public LevelChunk GetNextChunk(int i)
    {
        LevelChunk result = null;

        if (i == 0)
        {
            result = m_firstChunk;
        }
        else if (i == Size - 1)
        {
            result = m_lastChunk;
        }
        else
        {
            if (m_repeat == RepeatType.Random)
            {
                result = GetRndChunk();
            }
            else if (m_repeat == RepeatType.Repeat)
            {
                result = GetRepeatChunk(i);
            }
        }


        return result;
    }

    private LevelChunk GetRndChunk()
    {
        var rndIndex = Random.Range(0, m_chunks.Length);
        return m_chunks[rndIndex];
    }

    private LevelChunk GetRepeatChunk(int i)
    {
        i = (int)Mathf.Repeat(i - 1, m_chunks.Length);
        return m_chunks[i];
    }
}
