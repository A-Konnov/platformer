﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private ChunkSet m_set;
    public ChunkSet ChunkSet { get { return m_set; } }

    private void Awake()
    {
        GenerateLevel();
    }

    [ContextMenu("Clear")]
    private void Clear()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    [ContextMenu("Generate")]
    public void GenerateLevel()
    {
        Clear();

        if(m_set == null) { return; }

        var pos = Vector3.zero;
        for (int i = 0; i < m_set.Size; i++)
        { 
            var chunk = m_set.GetNextChunk(i);

            //вычисление смещения чанка
            var offset = Vector3.right * chunk.Width * 0.5f;

            if (i != 0)
            {
                pos += offset - new Vector3(chunk.OffsetX, 0, 0);
            }

            Instantiate(chunk, pos, Quaternion.identity, transform);
            pos += offset + new Vector3(chunk.OffsetX, 0, 0); 
        }
    }
}
