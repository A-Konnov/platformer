﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEllen : MonoBehaviour
{
    [SerializeField] private Transform m_spawner;
    [SerializeField] private GameObject m_ellen;

    private void Start()
    {
        Instantiate(m_ellen, m_spawner.position, m_spawner.rotation);
    }

}
