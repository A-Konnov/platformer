﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class ObjectsRequiringItems : MonoBehaviour
{
    [SerializeField] private InventoryItem[] m_items;    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var inventory = collision.GetComponent<InventoryController>();
        if (inventory != null)
        {
            if (inventory.HasItems(m_items))
            {
                foreach (var item in m_items)
                {
                    inventory.RemoveItem(item);
                }

                var activate = GetComponent<AActivate>();
                if (activate != null)
                {
                    activate.Activate();
                }
            }
        }
    }
}
