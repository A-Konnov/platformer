﻿using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
public class Platform2D : MonoBehaviour
{
    [SerializeField] private Vector2[] m_waypoints;
    [SerializeField] private float m_speed;

    private Rigidbody2D m_rb2D;
    private Vector3 m_startPoint;
    private ContactPoint2D[] m_contacts = new ContactPoint2D[8]; //выделяем память для массива контактов с платформой, 8 достаточно

    private void Start()
    {
        m_startPoint = transform.position;
        m_rb2D = GetComponent<Rigidbody2D>();

        var path = new Vector3[m_waypoints.Length]; //создаем массив
        for (int i = 0; i < m_waypoints.Length; i++)
        {
            path[i] = transform.position + (Vector3)m_waypoints[i]; //конвертируем в Vector3
        }

        transform.DOPath(path, m_speed) //двигаем по поинтам
            .SetSpeedBased(true)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo);
    }

    private void FixedUpdate()
    {
        ClearContacts();
        MoveContacts();
    }

    private void ClearContacts()
    {
        for (int i = 0; i < m_contacts.Length; i++)
        {
            if (m_contacts[i].rigidbody != null)
            {
                m_contacts[i].rigidbody.transform.SetParent(null); //убираем из чилдов
            }

            m_contacts[i] = new ContactPoint2D(); //стираем массив
        }
    }

    private void MoveContacts()
    {
        m_rb2D.GetContacts(m_contacts);
        foreach (var contact in m_contacts)
        {
            if (contact.rigidbody == null) continue; //continue - к следующиму элементу в foreach
            if (contact.rigidbody.position.y < m_rb2D.position.y) continue;
            contact.rigidbody.transform.SetParent(transform); //делаем чилдом
        }
    }

    private void OnDrawGizmos()
    {
        if (m_waypoints == null) return;

        var startPoint = transform.position;

        if (Application.isPlaying) //если проигрываем, то стратовая координата не меняется
        {
            startPoint = m_startPoint;
        }

        for (int i = 0; i < m_waypoints.Length - 1; i++)
        {
            var start = (Vector2)startPoint + m_waypoints[i];
            var end = (Vector2)startPoint + m_waypoints[i + 1];

            Gizmos.DrawSphere(start, 0.2f);
            Gizmos.DrawSphere(end, 0.2f);
            Gizmos.DrawLine(start, end);

        }
    }
}
