﻿using System.Collections;
using UnityEngine;

public class Dynamite : MonoBehaviour
{
    [Header("Property:")]
    [SerializeField] private float m_explosionDelay = 3f;
    [SerializeField] private float m_explosionRadius = 1.5f;
    [SerializeField] private float m_explosionDamage = 1f;

    [Header("Detonation Process")]
    [SerializeField] private Color m_colorDetonationFill = Color.red;
    [SerializeField] private float m_colorChangeTime = 0.2f;

    [Header("Reference:")]
    [SerializeField] private ParticleSystem m_fx;

    private Health m_hp;
    private SpriteRenderer m_sr;

    private void Start()
    {
        m_sr = GetComponent<SpriteRenderer>();
        m_hp = GetComponent<Health>();
        m_hp.OnHpChange.AddListener(StartExplosion);
    }

    private void StartExplosion()
    {
        Destroy(m_hp);
        m_hp.OnHpChange.RemoveListener(StartExplosion);

        StartCoroutine(Explosion());
        StartCoroutine(ChangeColor());
    }

    private IEnumerator Explosion()
    {   
        yield return new WaitForSeconds(m_explosionDelay);

        m_sr.enabled = false;

        var overlap = Physics2D.OverlapCircleAll(transform.position, m_explosionRadius);
        foreach (var obj in overlap)
        {
            var hp = obj.GetComponent<Health>();
            if (hp != null)
            {
                hp.ApplyDamage(m_explosionDamage);
            }
        }

        m_fx.Play();
        Destroy(gameObject, m_fx.main.duration);
    }

    private IEnumerator ChangeColor()
    {
        var delay = new WaitForSeconds(m_colorChangeTime);

        Color[] colors = new Color[2] { Color.white, m_colorDetonationFill };
        var colorNumber = 1;

        while (true)
        {
            yield return delay;
            m_sr.color = colors[colorNumber];
            colorNumber = colorNumber == 0 ? 1 : 0;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, m_explosionRadius);
    }
}
