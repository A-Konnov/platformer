﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TerminalActivate : AActivate
{
    [SerializeField] private GameObject m_door;
    [SerializeField] private float m_durationOpenDoor = 5f;
    [SerializeField] private float m_durationCameraMove = 5f;

    private Tracking m_camera;
    private PlayerController m_ellen;


    public override void Activate()
    {
        m_camera = FindObjectOfType<Tracking>();
        m_ellen = FindObjectOfType<PlayerController>();

        if (m_camera == null || m_ellen == null)
        { return; }

        Enabled(false);

        m_door.GetComponent<BoxCollider2D>().isTrigger = true;

        var spriteOpenDoor = m_door.transform.GetChild(0);
        var positionDoor = m_door.transform.position;
        var positionEllen = m_ellen.transform.position;
        positionDoor.z = m_camera.transform.position.z;
        positionEllen.z = m_camera.transform.position.z;

        var anim1 = m_camera.transform.DOMove(positionDoor, m_durationCameraMove);
        var anim2 = spriteOpenDoor.DORotate(new Vector3(0, 80, 0), m_durationOpenDoor);
        var anim3 = m_camera.transform.DOMove(positionEllen, m_durationCameraMove);
        var seq = DOTween.Sequence();
        seq.Append(anim1);
        seq.Append(anim2);
        seq.Append(anim3);
        seq.AppendCallback(() => Enabled(true));
        seq.Play();
    }

    private void Enabled(bool value)
    {
        m_camera.enabled = value;
        m_ellen.enabled = value;
    }
}

