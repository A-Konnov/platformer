﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AActivate : MonoBehaviour
{
    public abstract void Activate();
}
