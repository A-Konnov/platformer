﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxActivate : AActivate
{
    public override void Activate()
    {
        Destroy(gameObject);
    }
}
