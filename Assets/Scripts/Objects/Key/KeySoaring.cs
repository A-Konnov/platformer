﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class KeySoaring : MonoBehaviour
{
    [SerializeField] private float m_soaringHeight = 1f;
    [SerializeField] private float m_soaringSpeed = 0.5f;

    private void Start()
    {
        gameObject.transform.DOMoveY(transform.position.y + m_soaringHeight, m_soaringSpeed)
            .SetSpeedBased(true)
            .SetEase(Ease.InOutSine)
            .SetLoops(-1, LoopType.Yoyo);
    }

}
