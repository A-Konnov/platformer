﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Tracking : MonoBehaviour
{
    [SerializeField] private GameObject m_ellen;
    private Vector3 m_curPos;
    private Grid m_grid;
    private int m_totalWidth;
    private float m_leftBorderCamera;
    private float m_rightBorderCamera;


    private void Start()
    {
        gameObject.transform.parent = null;
        m_grid = FindObjectOfType<Grid>();
        LevelChunk[] tiles = m_grid.transform.GetComponentsInChildren<LevelChunk>(true);

        foreach (var tile in tiles)
        {
            m_totalWidth += tile.Width;
        }

        var gridCenter = (tiles[tiles.Length - 1].GetComponent<Tilemap>().cellBounds.center.x + tiles[tiles.Length - 1].transform.position.x) * 0.5f;
        var cameraWidth = Camera.main.aspect * 2f * Camera.main.orthographicSize;
        m_leftBorderCamera = (gridCenter - m_totalWidth * 0.5f) + (cameraWidth * 0.5f);
        m_rightBorderCamera = (gridCenter + m_totalWidth * 0.5f) - (cameraWidth * 0.5f);
    }

    private void Update()
    {
        //TODO Clamp
        //TODO y movement
        if (m_ellen.transform.position.x <= m_leftBorderCamera || m_ellen.transform.position.x >= m_rightBorderCamera)
        {
            return;
        }
        m_curPos = m_ellen.transform.position;
        m_curPos.z = -10;
        gameObject.transform.position = m_curPos;
    }

}
