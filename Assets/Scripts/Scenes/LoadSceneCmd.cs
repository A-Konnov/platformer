﻿using UnityEngine;

//паттерн команда
[CreateAssetMenu]
public class LoadSceneCmd : ScriptableObject
{
    public void LoadScene(string sceneName)
    {
        SceneLoader.LoadScene(sceneName);
    }
}
