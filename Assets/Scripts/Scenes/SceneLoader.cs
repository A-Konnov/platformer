﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private float m_delay;

    [Header("Animation:")]
    [SerializeField] private GameObject m_curtainTop;
    [SerializeField] private GameObject m_curtainBottom;
    [SerializeField] private GameObject m_skeleton;
    [SerializeField] private float m_duration;
    [SerializeField] private float m_speedSkeleton;

    private const string LOADING_SCENE = "Loading";
    private static string m_nextScene;
    private static bool m_isAsync;

    private void Awake()
    {
        m_curtainTop.transform.position = new Vector3(0, 8, 0);
        m_curtainBottom.transform.position = new Vector3(0, -8, 0);
    }

    public static void LoadScene(string sceneName, bool async = true)
    {
        m_nextScene = sceneName;
        m_isAsync = async;
        SceneManager.LoadScene(LOADING_SCENE);
    }

    private IEnumerator Start()
    {
        var anim1 = m_curtainTop.transform.DOMove(new Vector3(0, 3, 0), m_duration);
        var anim2 = m_curtainBottom.transform.DOMove(new Vector3(0, -3, 0), m_duration);
        var moveSkeletonLeft = m_skeleton.transform.DOMove(new Vector3(-10, 1.13f, 0), m_speedSkeleton).SetLoops(-1).SetEase(Ease.Linear);

        yield return new WaitForSeconds(m_delay);



        if (string.IsNullOrEmpty(m_nextScene))
        {
            Debug.LogError("Invalid scene name: " + m_nextScene);
            yield break;
        }

        if (!m_isAsync)
        {
            SceneManager.LoadScene(m_nextScene);
            yield break;
        }



        var loading = SceneManager.LoadSceneAsync(m_nextScene, LoadSceneMode.Additive);
        while (!loading.isDone)
        {
            yield return null;
        }

        Destroy(m_skeleton);
        m_curtainTop.transform.DOMove(new Vector3(0, 8, 0), m_duration);
        m_curtainBottom.transform.DOMove(new Vector3(0, -8, 0), m_duration).OnStepComplete(UnloadScene);
    }

    private static void UnloadScene()
    {
        SceneManager.UnloadSceneAsync(LOADING_SCENE);
    }
}
