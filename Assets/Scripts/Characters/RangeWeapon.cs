﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : MonoBehaviour
{
    [SerializeField] private RangeWeaponData m_weapon;
    [SerializeField] private Transform m_firePoint;

    private bool m_isReloading;

    public void Attack()
    {
        if (!enabled) return;

        if (m_isReloading) { return; }

        StartCoroutine(ProcessAttack());
    }

    private IEnumerator ProcessAttack()
    {
        m_isReloading = true;

        m_weapon.Attack(m_firePoint);

        yield return new WaitForSeconds(m_weapon.FireRate);
        m_isReloading = false;
    }
}
