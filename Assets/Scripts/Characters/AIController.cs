﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HorizontalMovement))]
public class AIController : MonoBehaviour
{
    [Header("Range Attack:")]
    [SerializeField] private bool m_rangeWeaponActive;
    [SerializeField] private float m_rangeAttackDelay = 0.2f;

    [Header("Melee Attack:")]
    [SerializeField] private bool m_meleeWeaponActive;

    [Header("Links:")]
    [SerializeField] private Animator m_animator;

    private HorizontalMovement m_movement;
    private MeleeWeapon m_meleeWeapon;
    private RangeWeapon m_rangeWeapon;
    private AIController[] m_AllAIController;

    private void Awake()
    {
        m_AllAIController = FindObjectsOfType<AIController>();
    }

    private void Start()
    {
        m_movement = GetComponent<HorizontalMovement>();
        m_meleeWeapon = GetComponent<MeleeWeapon>();
        m_rangeWeapon = GetComponent<RangeWeapon>();


        foreach (var AIcontr in m_AllAIController)
        {
            AIcontr.enabled = false;
        }
    }

    private void FixedUpdate()
    {
        m_movement.Move(Input.GetAxis("AIHorizontal"));
    }

    private void OnMouseOver()
    {
        foreach (var AIcontr in m_AllAIController)
        {
            AIcontr.enabled = false;
        }

        var AIController = GetComponent<AIController>();
        AIController.enabled = true;
    }

    private void Update()
    {      
        if (Input.GetButtonDown("Fire3")) //левый shift
        {
            if (m_rangeWeaponActive && m_rangeWeapon != null)
            {
                m_animator.SetTrigger("Attack");
                StartCoroutine(ProcessAttack());                
                return;
            }

            if (m_meleeWeaponActive && m_meleeWeapon != null)
            {
                m_animator.SetTrigger("Attack");
                m_meleeWeapon.Attack();
                return;
            }
        }
    }

    private IEnumerator ProcessAttack()
    {
        yield return new WaitForSeconds(m_rangeAttackDelay);
        m_rangeWeapon.Attack();
    }
}