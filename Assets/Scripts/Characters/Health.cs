﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] private float m_health = 1f;

    public float HP { get { return m_health; } }
    public UnityEvent OnHpChange = new UnityEvent();

    public void ApplyDamage(float damage)
    {
        m_health -= damage;
        OnHpChange.Invoke();
        if (m_health <= 0)
        {
            DestroyComponent<PlayerController>();


            var death = GetComponent<Death>();
            if (death != null)
            {
                death.Die();
            }
        }
    }

    private void DestroyComponent<T>() where T : Component
    {
        var component = GetComponent<T>();
        if (component != null)
        {
            Destroy(component);
        }
    }
}
