﻿using System;
using System.Collections;
using UnityEngine;


[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(HorizontalMovement))]
public class EnemyController : MonoBehaviour
{
    [SerializeField] private float m_movementRange = 5f;
    [SerializeField] private float m_moveToTargetRange = 5f;
    [SerializeField] private float m_acceleration = 0.5f;
    [SerializeField] private float m_timeStand = 2f;
    private MeleeWeapon m_weapon;
    private HorizontalMovement m_movement;
    private float m_speed;
    private bool m_stand;

    private Vector2[] m_waypoints;
    private int m_currWaypointIndex;

    private void Start()
    {
        m_stand = false;
        m_speed = 0;
        m_weapon = GetComponent<MeleeWeapon>();
        m_movement = GetComponent<HorizontalMovement>();

        m_waypoints = new Vector2[]
        {
            transform.position,
            transform.position + Vector3.left * m_movementRange
        };
    }

    private void FixedUpdate()
    {
        if (IsCanAttack())
        {
            m_weapon.Attack();
            return;
        }

        if (IsMoveToTarget()) return;

        if (m_stand) return;

        MoveThowWaypoints(Time.fixedDeltaTime);
    }

    private bool IsCanAttack()
    {
        if (DetectPlayer(m_weapon.WeaponData.Range) != null) return true;
        return false;
    }

    private bool IsMoveToTarget()
    {
        var player = DetectPlayer(m_moveToTargetRange);
        if (player != null)
        {
            Move(player.position);
            return true;
        }
        return false;
    }

    private Transform DetectPlayer(float range)
    {
        var origin = m_weapon.FirePoint.position;
        var direction = m_weapon.FirePoint.right;
        var mask = m_weapon.WeaponData.Mask;

        var hit = Physics2D.Raycast(origin, direction, range, mask);
        if (hit.collider != null)
        {
            var player = hit.collider.GetComponent<PlayerController>();
            if (player != null) return hit.collider.transform;
        }
        return null;
    }

    //Движение от точке к точке
    private void MoveThowWaypoints(float dt)
    {
        if (Mathf.Abs(m_movementRange) < float.Epsilon) return; //почти или менее нуля

        var point = m_waypoints[m_currWaypointIndex];
        Move(point);

        var frameSpeed = m_movement.Speed * dt; //скорость на фрейм
        if (Mathf.Abs(point.x - transform.position.x) < frameSpeed)
        {
            m_speed = 0;
            m_movement.Move(m_speed);
            m_currWaypointIndex = m_currWaypointIndex == 0 ? 1 : 0;
            StartCoroutine(Stand());
        }
    }

    private IEnumerator Stand()
    {
        m_stand = true;
        yield return new WaitForSeconds(m_timeStand);
        m_stand = false;
    }

    private void Move(Vector2 point)
    {
        var moveDir = point.x > transform.position.x ? 1f : -1f;
        moveDir = (moveDir * Time.deltaTime) * m_acceleration;

        m_speed = Mathf.Clamp(m_speed + moveDir, -1, 1);
        m_movement.Move(m_speed);
    }

    private void OnDrawGizmosSelected()
    {
        //отрисовка движения по точкам
        var point1 = transform.position;
        var point2 = transform.position + Vector3.left * m_movementRange;

        if (Application.isPlaying)
        {
            point1 = m_waypoints[0];
            point2 = m_waypoints[1];
        }

        point1.y += 0.5f;
        point2.y += 0.5f;

        Gizmos.DrawSphere(point1, 0.2f);
        Gizmos.DrawSphere(point2, 0.2f);
        Gizmos.DrawLine(point1, point2);

        //отрисовка moveToTarget
        m_weapon = GetComponent<MeleeWeapon>();

        if (m_weapon != null)
        {
            var origin = m_weapon.FirePoint.position;
            origin.y = origin.y + 0.2f;
            var ray = m_weapon.FirePoint.right * m_moveToTargetRange;

            Gizmos.color = Color.red;
            Gizmos.DrawRay(origin, ray);
        }
    }


}
