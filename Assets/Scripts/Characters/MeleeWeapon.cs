﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MeleeWeapon : MonoBehaviour
{
    [SerializeField] private MeleeWeaponData m_weapon;
    [SerializeField] private Transform m_firePoint;

    public bool IsAttacking { get; private set; }
    public Transform FirePoint { get { return m_firePoint; } }
    public MeleeWeaponData WeaponData { get { return m_weapon; } }

    public UnityEvent AttackStarted = new UnityEvent();
    public UnityEvent AttackCompleted = new UnityEvent();

    public void Attack()
    {
        if (!enabled) return;

        if (IsAttacking) { return; }

        StartCoroutine(ProcessAttacking());
    }

    private IEnumerator ProcessAttacking()
    {
        IsAttacking = true;
        AttackStarted.Invoke();

        yield return new WaitForSeconds(m_weapon.HitDelay); //ждем анимацию удара

        m_weapon.Attack(m_firePoint); //ударяем

        yield return new WaitForSeconds(m_weapon.FireRate - m_weapon.HitDelay); //пауза между ударами, не учитывая время анимации

        IsAttacking = false;
        AttackCompleted.Invoke();
    }

    private void OnDrawGizmosSelected() //рисуем гизму оружия
    {
        if (m_weapon != null && m_firePoint != null)
        {
            var origin = m_firePoint.position;
            var ray = m_firePoint.right * m_weapon.Range;

            Gizmos.color = Color.green;
            Gizmos.DrawRay(origin, ray);
        }
    }
}
