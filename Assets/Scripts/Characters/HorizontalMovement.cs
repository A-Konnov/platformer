﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HorizontalMovement : MonoBehaviour
{
    [SerializeField] private float m_speed = 5f;
    [SerializeField] private GameObject m_art;

    public float Speed { get { return m_speed; } }
    private Rigidbody2D m_rb2D;

    public Vector2 Velocity
    {
        get { return m_rb2D.velocity; }
    }

    private void OnEnable()
    {
        if (m_rb2D == null)
        {
            m_rb2D = GetComponent<Rigidbody2D>();
        }
    }

    public void Move(float axis)
    {
        if (!enabled) return;

        var velocity = m_rb2D.velocity;
        velocity.x = axis * m_speed;
        m_rb2D.velocity = velocity;

        if (Mathf.Abs(axis) > float.Epsilon) //проверяем, что есть скорость по модулю (Epsilon число близское к ноль)
        {
            var rot = axis > 0 ? 0 : 180;  //если вправо, то 0, есди влево то 180   
            m_art.transform.rotation = Quaternion.Euler(0, rot, 0); //поворачиваем по направлению
        }
    }
}