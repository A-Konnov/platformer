﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Jumping : MonoBehaviour
{
    [Header("Jumping:")]
    [SerializeField]
    private float m_jumpForce = 15f;

    [Header("Grounding:")]
    [SerializeField]
    private float m_rayDistance = 0.2f;
    [SerializeField] private LayerMask m_mask;
    [SerializeField] private float m_groundingDelay = 0.2f;

    private Rigidbody2D m_rb2D;
    private Collider2D m_collider2D;

    private float m_timeSinceJumpStarted; //время от начала прыжка

    public bool IsGrounded { get; private set; }

    private void Start()
    {
        m_rb2D = GetComponent<Rigidbody2D>();
        m_collider2D = GetComponent<Collider2D>();
        m_timeSinceJumpStarted = m_groundingDelay;
        IsGrounded = true;
    }

    private void FixedUpdate()
    {
        m_timeSinceJumpStarted += Time.fixedDeltaTime;
        CheckGrounding();
    }

    private void CheckGrounding()
    {
        if (m_timeSinceJumpStarted < m_groundingDelay) //если время от прыжка маленькое, то return
        {
            return;
        }

        var y = m_collider2D.bounds.min.y;
        var x = m_collider2D.bounds.center.x;

        var hit = Physics2D.Raycast(new Vector2(x, y), Vector2.down, m_rayDistance, m_mask); //кидаем луч вниз
        if (m_rb2D.velocity.y <= 0)
        {
            IsGrounded = hit.collider != null;  //true, если есть снизу коллайдер
        }
    }

    public void Jump()
    {
        if (!enabled) return;

        if (!IsGrounded) return;

        IsGrounded = false;
        m_timeSinceJumpStarted = 0;

        m_rb2D.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
    }

}

//TODO  если скорость о Y больше 0, то не делать прыжок