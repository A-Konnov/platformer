﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class FreezeOnHit : MonoBehaviour
{
    [SerializeField] private float m_freezeTime = 1;
    private Health m_hp;

    private void Start()
    {
        m_hp = GetComponent<Health>();
        m_hp.OnHpChange.AddListener(Freeze);
    }

    private void OnDestroy()
    {
        m_hp.OnHpChange.RemoveListener(Freeze);
    }

    private void Freeze()
    {
        if (m_hp.HP <= 0) return;
        StartCoroutine(FreezeCoroutine());
    }

    private IEnumerator FreezeCoroutine()
    {
        EnableComponent<PlayerController>(false);
        EnableComponent<EnemyController>(false);

        yield return new WaitForSeconds(m_freezeTime);

        EnableComponent<PlayerController>(true);
        EnableComponent<EnemyController>(true);
    }

    private void EnableComponent<T> (bool enable) where T : MonoBehaviour
    {
        var comp = GetComponent<T>();
        if (comp != null)
        {
            comp.enabled = enable;
        }
    }
}
