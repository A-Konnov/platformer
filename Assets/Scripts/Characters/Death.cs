﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    [SerializeField] private float m_timeToDestroy = 3f;

    public void Die()
    {
        var rb2D = GetComponent<Rigidbody2D>();
        rb2D.simulated = false;

        m_animator.SetTrigger("Death");
        StartCoroutine(Destroy());
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(m_timeToDestroy);
        Destroy(gameObject);
    }
}
